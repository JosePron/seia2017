import datetime
from haystack import indexes
from .models import Receta


class RecetaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    usuario = indexes.CharField(model_attr='usuario')
    titulo = indexes.CharField(model_attr='titulo')
    fecha = indexes.DateTimeField(model_attr='fecha')
    descripcion = indexes.CharField(model_attr='descripcion')

    def get_model(self):
        return Receta

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(fecha__lte=datetime.datetime.now())
