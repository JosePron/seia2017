from functools import wraps
from django.http import HttpResponseBadRequest, JsonResponse
from django.conf import settings


def ajax_login_required(view_func):
   @wraps(view_func)
   def wrapper(request, *args, **kwargs):
       if request.user.is_authenticated():
           return view_func(request, *args, **kwargs)
       response = {'not_authenticated': True,
                   'redirect': settings.LOGIN_URL}
       return JsonResponse(response, safe=False)
   return wrapper