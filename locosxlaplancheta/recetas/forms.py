# -*- coding: utf-8 -*-

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div
from crispy_forms.bootstrap import PrependedText

from .models import Receta, Paso, PedidoReceta



class RecetaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RecetaForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
       
    class Meta:
        model = Receta
        fields = ['categoria', 'titulo', 'descripcion', 'ingredientes', 'img']


class PasoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PasoForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

    class Meta:
        model = Paso
        fields = ['descripcion', 'img']

class PedidoRecetaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PedidoRecetaForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
       
    class Meta:
        model = PedidoReceta
        fields = ['titulo', 'descripcion']

RecetaPasoFormSet = forms.inlineformset_factory(Receta, Paso, form=PasoForm, extra=1, can_delete=True)