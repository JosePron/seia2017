from moderation import moderation
from moderation.db import ModeratedModel

from .models import Receta


#class AnotherModelModerator(ModelModerator):
    # Add your moderator settings for AnotherModel here


moderation.register(Receta)  # Uses default moderation settings
#moderation.register(AnotherModel, AnotherModelModerator)  # Uses custom moderation settings