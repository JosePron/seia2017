# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import *


urlpatterns = [
   url(r'^$', RecetaListView.as_view(), name='index'),
   url(r'^categorias/$', CategoriaListView.as_view(), name='categorias'),
   url(r'^categorias/(?P<pk>.+)/$', RecetaCategoriaListView.as_view(), name='receta_categoria'),
   url(r'^recetas/usuario/(?P<pk>.+)/$', RecetaUsuarioListView.as_view(), name='receta_usuario'),
   url(r'^recetas/crear/$', RecetaCreateView.as_view(), name='receta_create'),
   url(r'^recetas/(?P<pk>.+)/editar/$', RecetaUpdateView.as_view(), name='receta_edit'),
   url(r'^recetas/(?P<pk>.+)/eliminar/$', RecetaDeleteView.as_view(), name='receta_delete'),
   url(r'^recetas/(?P<pk>.+)/$', RecetaDetailView.as_view(), name='receta_detail'),
   url(r'^ajax/voteUpReceta/$', voteUpReceta, name='voteUpReceta'),
   url(r'^ajax/voteDownReceta/$', voteDownReceta, name='voteDownReceta'),
   url(r'^pedidorecetas/usuario/$', PedidoRecetaUsuarioListView.as_view(), name='pedidoreceta_usuario'),
   url(r'^pedidorecetas/crear/$', PedidoRecetaCreateView.as_view(), name='pedidoreceta_create'),
   url(r'^pedidorecetas/(?P<pk>.+)/$', PedidoRecetaDetailView.as_view(), name='pedidoreceta_detail'),

]