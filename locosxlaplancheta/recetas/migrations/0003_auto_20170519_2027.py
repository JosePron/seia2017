# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-19 23:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recetas', '0002_auto_20170514_2153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receta',
            name='status',
            field=models.TextField(choices=[('publicada', 'Publicada'), ('suspendida', 'Suspendida'), ('eliminada', 'Eliminada')]),
        ),
    ]
