# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Receta, Categoria, User, PedidoReceta
from .forms import RecetaPasoFormSet, RecetaForm, PedidoRecetaForm
from django.template import RequestContext
from django.http import JsonResponse
from .Utils import *

class RecetaListView(ListView):
    #model = Receta.objects.get(status = Receta.STATUS_PUBLICADA)
    template_name = 'index.html'

    def get_queryset(self):
        return Receta.objects.filter(status = Receta.STATUS_PUBLICADA).order_by('-votes')

    def get_context_data(self, **kwargs):
        context = super(RecetaListView, self).get_context_data(**kwargs)
        return context


class RecetaDetailView(DetailView):
    model = Receta
    template_name = 'recetas/receta_detail.html'

    def get_context_data(self, **kwargs):
        context = super(RecetaDetailView, self).get_context_data(**kwargs)
        return context


class RecetaCreateView(LoginRequiredMixin, CreateView):
    model = Receta
    form_class = RecetaForm
    template_name = 'recetas/receta_create.html'
    success_url = '/'

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        pasos_form = RecetaPasoFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  pasos_form=pasos_form))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        pasos_form = RecetaPasoFormSet(self.request.POST, self.request.FILES)
        if (form.is_valid() and pasos_form.is_valid()):
            return self.form_valid(form, pasos_form)
        else:
            return self.form_invalid(form, pasos_form)

    def form_valid(self, form, pasos_form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        self.object = form.save(commit=False)
        self.object.usuario = self.request.user
        self.object.status = Receta.STATUS_PUBLICADA
        self.object.save()
        pasos_form.instance = self.object
        pasos_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, pasos_form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form, pasos_form=pasos_form))


class RecetaUpdateView(LoginRequiredMixin, UpdateView):
    model = Receta
    form_class = RecetaForm
    template_name = 'recetas/receta_create.html'
    success_url = '/'

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = self.get_object()
        print self.object
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        pasos_form = RecetaPasoFormSet(instance=self.get_object())
        return self.render_to_response(
            self.get_context_data(form=form,
                                  pasos_form=pasos_form))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        pasos_form = RecetaPasoFormSet(self.request.POST, self.request.FILES, instance=self.get_object())
        if (form.is_valid() and pasos_form.is_valid()):
            return self.form_valid(form, pasos_form)
        else:
            return self.form_invalid(form, pasos_form)

    def form_valid(self, form, pasos_form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        self.object = form.save(commit=False)
        self.object.usuario = self.request.user
        self.object.save()
        pasos_form.instance = self.object
        pasos_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, pasos_form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form, pasos_form=pasos_form))


class RecetaDeleteView(LoginRequiredMixin, DeleteView):
    model = Receta
    template_name = 'recetas/receta_delete.html'
    success_url = '/'

class CategoriaListView(ListView):
    model = Categoria
    template_name = 'categorias.html'

    def get_context_data(self, **kwargs):
        context = super(CategoriaListView, self).get_context_data(**kwargs)
        return context

class RecetaCategoriaListView(ListView):
    template_name = 'recetas/receta_categoria.html'

    def get_queryset(self):
    	categoria = Categoria.objects.get(id=self.kwargs['pk']) #self.kwargs['pk'] es el parametros
        return (Receta.objects.filter(categoria = categoria)).filter(status = Receta.STATUS_PUBLICADA)

    def get_context_data(self, **kwargs):
    	context = super(RecetaCategoriaListView, self).get_context_data(**kwargs)
        context['Categoria'] = Categoria.objects.get(id=self.kwargs['pk'])
        return context

class RecetaUsuarioListView(ListView):
    template_name = 'recetas/receta_usuario.html'

    def get_queryset(self):
    	usuario = User.objects.get(id = self.kwargs['pk'])
    	return (Receta.objects.filter(usuario = usuario)).filter(status = Receta.STATUS_PUBLICADA)

    def get_context_data(self, **kwargs):
    	context = super(RecetaUsuarioListView, self).get_context_data(**kwargs)
        context['Usuario'] = User.objects.get(id=self.kwargs['pk'])
        return context   

@ajax_login_required 
def voteUpReceta(request):
    usuario = request.GET.get('usuario', None)
    id_receta = request.GET.get('id_receta', None)
    receta = Receta.objects.get(pk = id_receta)
    receta.votes.up(usuario)
    data = {
        'votos': receta.votes.count()
    }
    return JsonResponse(data)

@ajax_login_required 
def voteDownReceta(request):
    usuario = request.GET.get('usuario', None)
    id_receta = request.GET.get('id_receta', None)
    receta = Receta.objects.get(pk = id_receta)
    receta.votes.down(usuario)
    data = {
        'votos': receta.votes.count()
    }
    return JsonResponse(data)

class PedidoRecetaDetailView(DetailView):
    model = PedidoReceta
    template_name = 'recetas/pedidoreceta_detail.html'

    def get_context_data(self, **kwargs):
        context = super(PedidoRecetaDetailView, self).get_context_data(**kwargs)
        return context


class PedidoRecetaCreateView(LoginRequiredMixin, CreateView):
    model = PedidoReceta
    form_class = PedidoRecetaForm
    template_name = 'recetas/pedidoreceta_create.html'
    success_url = '/'

    def form_valid(self, form):
        """
        Called if all forms are valid. Creates a Recipe instance along with
        associated Ingredients and Instructions and then redirects to a
        success page.
        """
        self.object = form.save(commit=False)
        self.object.usuario = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form))

class PedidoRecetaUsuarioListView(ListView):
    template_name = 'recetas/pedidoreceta_usuario.html'

    def get_queryset(self):
        return PedidoReceta.objects.order_by('-fecha')
 
