# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.encoding import smart_unicode
from django.contrib.auth.models import User
from django.db import models
from django.core.urlresolvers import reverse
from vote.models import VoteModel

class Status(models.Model):

    nombre = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Status"
        verbose_name_plural = "Status"

    def __unicode__(self):
        return smart_unicode(self.nombre)


class Categoria(models.Model):

    nombre = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"

    def __unicode__(self):
        return smart_unicode(self.nombre)


class Receta(VoteModel, models.Model):

    STATUS_PUBLICADA = 'publicada'
    STATUS_SUSPENDIDA = 'suspendida'
    STATUS_ELIMINADA = 'eliminada'

    STATUS_CHOICES = (
        (STATUS_PUBLICADA, 'Publicada'),
        (STATUS_SUSPENDIDA, 'Suspendida'),
        (STATUS_ELIMINADA, 'Eliminada')
    )

    usuario = models.ForeignKey(User, related_name="recetas")
    categoria = models.ForeignKey(Categoria, related_name="recetas_categoria")
    status = models.TextField(choices=STATUS_CHOICES)
    titulo = models.CharField(max_length=150)
    descripcion = models.TextField()
    fecha = models.DateField(auto_now_add=True)
    ingredientes = models.TextField()
    img = models.ImageField(upload_to=None)

    class Meta:
        verbose_name = "Receta"
        verbose_name_plural = "Recetas"

    def __unicode__(self):
        return smart_unicode(self.titulo)

    def get_absolute_url(self):
        return reverse('receta_detail', kwargs={"pk":self.pk})


class Paso(models.Model):

    receta = models.ForeignKey(Receta, related_name='pasos')
    descripcion = models.TextField()
    img = models.ImageField(upload_to=None)

    class Meta:
        verbose_name = "Paso"
        verbose_name_plural = "Pasos"

    def __unicode__(self):
        return smart_unicode('{} {}'.format(self.pk, self.receta.titulo))

    
class PedidoReceta(models.Model):

    STATUS_PUBLICADA = 'publicada'
    STATUS_SUSPENDIDA = 'suspendida'
    STATUS_ELIMINADA = 'eliminada'

    STATUS_CHOICES = (
        (STATUS_PUBLICADA, 'Publicada'),
        (STATUS_SUSPENDIDA, 'Suspendida'),
        (STATUS_ELIMINADA, 'Eliminada')
    )

    usuario = models.ForeignKey(User, related_name="pedidorecetas")
    status = models.TextField(choices=STATUS_CHOICES)
    titulo = models.CharField(max_length=150)
    descripcion = models.TextField()
    fecha = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "PedidoReceta"
        verbose_name_plural = "PedidoRecetas"

    def __unicode__(self):
        return smart_unicode(self.titulo)

    def get_absolute_url(self):
        return reverse('pedidoreceta_detail', kwargs={"pk":self.pk})
