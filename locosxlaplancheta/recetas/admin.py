# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

from moderation.admin import ModerationAdmin



@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    pass


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    pass


class PasoInlineAdmin(admin.StackedInline):
    model = Paso

@admin.register(Receta)
class RecetaAdmin(ModerationAdmin):
    inlines = [ PasoInlineAdmin, ]
    list_display = ['fecha', 'titulo', 'categoria', 'status', 'usuario']
    list_filter = ('usuario', 'status', 'categoria')  # FIXME
    search_fields = ['titulo']