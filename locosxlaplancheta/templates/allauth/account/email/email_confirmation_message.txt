
{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Hola!

Usted recibió este mensaje porque el usuario {{ user_display }}, nos ha dado esta dirección de e-mail para crear una cuenta en nuestro sitio web.

Si esto es correcto haga click en el siguiente enlace para activar la cuenta --> {{ activate_url }}

De lo contrario obvie este mensaje.

Saludos!
{% endblocktrans %}{% endautoescape %}
LocosXlaPlancheta